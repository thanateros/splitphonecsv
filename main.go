package main

import (
	"bufio"
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

func insert(s []string, ele string, i int) []string {
	fmt.Printf("s: %+v\n", strings.Join(s, ", "))
	s = append(s, "")
	copy(s[i+1:], s[i:])
	s[i] = ele
	fmt.Printf("new s: %+v\n\n", strings.Join(s, ", "))
	return s
}

func main() {
	const LAND_LINE = 5
	const FAX = 6
	const MOBILE = 8
	lineNo := 0
	extractedCSV := [][]string{}
	cellValue := ""
	outputRow := []string{}

	phAndExtensionRegex := regexp.MustCompile(`(\d{3})\D{0,2}(\d{3})\D{0,2}(\d{4})\D*(\d*)`)
	cellRegex := regexp.MustCompile(`(?i)(cell|mobile|c:)+`)

	inputCSVFilePtr := flag.String("file", "", "path to input CSV file that needs phone numbers massaged after passing through the name splitter program.\n Takes input columns 5 (land line), 6 (fax), and 8 (cell) -- 0-based index -- and scrubs the number to conform to xxx-xxx-xxxx; splits out the extension on the land line; if either of 5 or 6 has any capitalization-variation of the words 'cell', 'mobile', or 'c:' then it blanks that field and moves the scrubbed value to the new cell phone column.\nThe result is ..... land-line | extension | fax | mobile/cell ...\nExample: 'splitphoneCSV -file path/to/csv/file'. \nOutput has '_phonefix.csv' at end of original file name.\n")
	flag.Parse()

	outputCSV := strings.TrimSuffix(*inputCSVFilePtr, filepath.Ext(*inputCSVFilePtr)) + "_phonefix.csv"

	csvFile, _ := os.Open(*inputCSVFilePtr)
	defer csvFile.Close()

	reader := csv.NewReader(bufio.NewReader(csvFile))

	for {
		line, err := reader.Read()

		if err == io.EOF {
			break
		} else if err != nil {
			fmt.Printf("\nThere was an error: %v\n", err)
			fmt.Println("Aborting. \nType 'extractCSV -h' for assistance.")
			os.Exit(1)
		}

		for i := 0; i < len(line); i++ {
			parts := []string{}
			isCell := false

			if i == LAND_LINE {
				if phAndExtensionRegex.MatchString(line[i]) {
					parts = phAndExtensionRegex.FindStringSubmatch(line[i])
					isCell = cellRegex.MatchString(line[i])
					if isCell {
						cellValue = fmt.Sprintf("%v-%v-%v", parts[1], parts[2], parts[3])
						outputRow = append(outputRow, "", "")
						fmt.Printf("LINE: %v has no Land Line #\n", lineNo)
						isCell = false
						continue
					}
					outputRow = append(outputRow, fmt.Sprintf("%v-%v-%v", parts[1], parts[2], parts[3]), parts[4])
					continue
				} else {
					outputRow = append(outputRow, "", "")
					fmt.Printf("LINE: %v has no Land Line #\n", lineNo)
					continue
				}
			}

			if i == FAX {
				if phAndExtensionRegex.MatchString(line[i]) {
					parts = phAndExtensionRegex.FindStringSubmatch(line[i])
					isCell = cellRegex.MatchString(line[i])
					if isCell {
						cellValue = fmt.Sprintf("%v-%v-%v", parts[1], parts[2], parts[3])
						outputRow = append(outputRow, "")
						fmt.Printf("LINE: %v has no Fax #\n", lineNo)
						isCell = false
						continue
					}
					outputRow = append(outputRow, fmt.Sprintf("%v-%v-%v", parts[1], parts[2], parts[3]))
					continue
				} else {
					outputRow = append(outputRow, "")
					fmt.Printf("LINE: %v has no Fax #\n", lineNo)
					continue
				}
			}

			if i == MOBILE {
				if phAndExtensionRegex.MatchString(line[i]) {
					parts = phAndExtensionRegex.FindStringSubmatch(line[i])
					outputRow = insert(outputRow, fmt.Sprintf("%v-%v-%v", parts[1], parts[2], parts[3]), i)
					continue
				} else if cellValue != "" {
					outputRow = insert(outputRow, cellValue, i)
					continue
				} else {
					outputRow = insert(outputRow, "", i)
					continue
				}
			}

			outputRow = append(outputRow, line[i])
		}

		extractedCSV = append(extractedCSV, outputRow)
		outputRow = []string{}
		cellValue = ""
		fmt.Println()

		lineNo++
	}

	fmt.Println("\n Finished scrubbing and splitting land, fax, and mobile columns.\nPreparing to write new file.")

	outputFile, err := os.Create(outputCSV)
	checkError("Cannot create file: ", err)
	defer outputFile.Close()

	writer := csv.NewWriter(outputFile)
	defer writer.Flush()

	for _, value := range extractedCSV {
		err := writer.Write(value)
		checkError("Cannot write to file: ", err)
	}

	fmt.Printf("==========\nCompleted scrubbing and splitting phone columns.\nNew file generated: %v\n==========\n", outputCSV)
}

func checkError(message string, err error) {
	if err != nil {
		log.Fatal(message, err)
	}
}
